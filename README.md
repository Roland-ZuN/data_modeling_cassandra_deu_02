# **README** <!-- omit in toc -->
# Data Modeling with Cassandra <!-- omit in toc -->

<!-- Original image size [1280 858] -->
<div align="center">
  <img width="256" height="171" src="img/cassandra_logo.png"/>
</div>
<br/><br/> <!-- Blank line -->

Model user activity data to create a NoSQL database and **ETL**
(**Extract, Transform, Load**) pipeline in Apache Cassandra for a music
streaming app.

## **Table of Content** <!-- omit in toc -->
- [Introduction](#introduction)
- [Project Description](#project-description)
- [Project Development](#project-development)
  - [Datasets](#datasets)
    - [Download the Datasets](#download-the-datasets)
    - [Dataset Tree Directory](#dataset-tree-directory)
  - [Local Database Setup](#local-database-setup)
  - [Development Files](#development-files)
  - [Setup Development Environment](#setup-development-environment)
    - [Create Environment](#create-environment)
    - [Create Jupyter Kernel Environment](#create-jupyter-kernel-environment)
    - [Package Requirements](#package-requirements)
- [Expected Results](#expected-results)
  - [Query 1](#query-1)
  - [Query 2](#query-2)
  - [Query 3](#query-3)

<br/><br/> <!-- Blank line -->

# Introduction

A startup called _**Sparkify**_ wants to analyze the data they have been
collecting on songs and user activity on their new music streaming app.
The analytics team is particularly interested in understanding **what
songs users are listening to**. Currently, they do not have an easy way
to query the **data** to generate the desired results, since the data
**resides in a directory of `CSV` files** on user activity on the app.

It is required an **Apache Cassandra database** which can create queries
on song play data to answer the questions. The current proect consists
in creating the database to accomplish the required analysis.

The database and ETL process are requited to be tested by running given
queries by the analytics team from Sparkify and comparing the outputs
with the expected results.

<br/><br/> <!-- Blank line -->

# Project Description

In the project is applied knowledge about **data modeling** with
Apache Cassandra and creation of an **ETL pipeline** using Python.
Tables are designed to run specific queries.

<br/><br/> <!-- Blank line -->

# Project Development

To develop all the project, it is already defined the queries, but no
the complete database structure, also, the raw data is only available in
`CSV` files, and a ETL pipeline is required, the following sections
describe how the project is handled.

<br/><br/> <!-- Blank line -->

## Datasets

To understand the requirements, business purposes and technical
characteristics of the project the first step is get the dataset into
the development environment.

<br/><br/> <!-- Blank line -->

### Download the Datasets

1. Access to Udacity's Project Workspace and open a `Terminal`.

<!-- Original image size [1369 820] -->
<div align="center">
  <img width="547" height="328" src="img/udacity_workspace_1.png" />
</div>
<br/><br/> <!-- Blank line -->

2. Inside the terminal type the following command:
   ```bash
   zip -r event_data.zip event_data
   ```

3. In the workspace directory (`/home/workspace`) a new file will appear
   and it is called `event_data.zip`, download the file.

   <!-- Original image size [638 618] -->
<div align="center">
  <img width="491" height="475" src="img/download_dataset.png" />
</div>
<br/><br/> <!-- Blank line -->

4. The `event_data.zip` is also available in a personal
   [drive](https://drive.google.com/file/d/11GOA401ZD7RHs4c-ONOK3_Gd7-WsiX_u/view?usp=sharing)
   to be download.

<br/><br/> <!-- Blank line -->

### Dataset Tree Directory

Locate the `event_data` directory in the root path of the development
environment, all the `CSV` files are located here, the following diagram
shows the current directory tree, and below it, there are two file path
examples to the `CSV` files.

```bash
<root_path>/data_modeling_cassandra_deu_02/event_data
    2018-11-01-events.csv
    2018-11-02-events.csv
    2018-11-03-events.csv
    ...
    2018-11-28-events.csv
    2018-11-29-events.csv
    2018-11-30-events.csv
```
<br/><br/> <!-- Blank line -->

* `<root_path>/data_modeling_cassandra_deu_02/event_data/2018-11-01-events.csv`
* `<root_path>/data_modeling_cassandra_deu_02/event_data/2018-11-30-events.csv`

To look at the `CSV` data  create a pandas dataframe to read the data
(code example below), after the code block example, there is an example
of what the data (`2018-11-18-events.csv`) looks like. 

<br/><br/> <!-- Blank line -->

```python
import os
import pandas as pd

file_path = os.path.join(
    os.getcwd(), "event_data/2018-11-18-events.csv"
)
events_df = pd.read_csv(file_path)
events_df.head()
```
<br/><br/> <!-- Blank line -->

<!-- Original image size [1133 378] -->
<div align="center">
  <img width="917" height="306" src="img/event_data_sample.png" />
</div>
<br/><br/> <!-- Blank line -->

It is important to identify what are the content of the available files,
the name of the attributes helps to know the type of data to store, for 
example, the `page` attribute and values indicates that is possible to
have only specific values and no numbers, just strings, so at designing
the database a good type of data could be `varchar` or `text`.

It is a recommendation to **visualize more than one** `CSV` file, it
helps to understand data better and identify possible inconsistencies or
common missing values.

> **Comments**:
> 
> * Use this
>   [JSON file format (video)](https://www.youtube.com/watch?time_continue=1&v=hO2CayzZBoA).
>   resource to better understand the JSON files.
> * **HINT**: Use the `value_counts` method on log dataframes, this is a
>   good option to identify attributes that store only specific values,
>   e.g:
> 
>     ```python
>           df["attribute_n"].value_counts()
>     ```
> <br/><br/> <!-- Blank line -->
<br/><br/> <!-- Blank line -->

## Local Database Setup

1. Make sure the **Apache Cassandra for Windows v.3.11.11**  is
   installed (follow instructions instalation on this 
   [link](https://phoenixnap.com/kb/install-cassandra-on-windows),
    and make sure to install the
   [Java SE Development Kit 8u251](https://www.oracle.com/java/technologies/javase/javase8u211-later-archive-downloads.html)
   before, because it is the version required to correctly install
   Cassandra).
2. Open the Cassandra  installation path.
3. Run a `Command Prompt` window in the same path.
4. Type the following command:
   
   ```bash
   cassandra
   ```
   
5. To verify if the database is installed correctly, open a new
   `Command Prompt` window in the same location.

6. Type the following:
   
   ```CQL
   DESC KEYSPACES
   ```

7. The command will show the different keyspaces created on the local
   node.

8. Do not close the first `Command Prompt` window, it is required
   running to execute the Python-Cassandra package in the Jupyter
   Notebook.

<br/><br/> <!-- Blank line -->

## Development Files

The development include the following scripts:

1. `Data_Modeling_With_Cassamdra.ipynb`: The full project documentation,
   it indicates how to perform the ETL process, how the tables were
   designed, tables creation, insert data and queries to answer the
   especified questions.
   <br><br> <!-- Blank line -->

2. `requirements.txt`> Contains the packages and versions used to run
   the project locally.

Inside each file there are the corresponding docstrings and execution
description.

<br><br> <!-- Blank line -->

## Setup Development Environment

The current project is developed in **Anaconda** installed on a
**Windows 10** OS; use the corresponding Python version
(**Python v.3.9.6 64-bit**).

* Check the Python version in the current environment using the
  following command line (it is assumed Miniconda or Anaconda is
  installed):

  `python --version`

* To verify the Python architecture installed use the following command:

  `python -c "import struct; print(struct.calcsize('P')*8)"`

  The number returned correspond to the architecture (*32* for *32-bits*
  and *64* for *64-bits*).

It is important to **have the same Python version** to reduce
incompatibilities when exporting the scripts from one environment to
other, the following sections help to create a Python Virtual
environment on case the Python version is not available in an existing
virtual environment.
<br><br> <!-- Blank line -->

### Create Environment

An environment helps to avoid packages conflicts, and it allows
developing software under similar production conditions.

_**The environment is set using Anaconda Python package distribution.**_

* Create a new environment:

  `conda create -n data_engineer python=3.9.6`

* Install pip into the new environment:

  `conda install -n data_engineer pip`

* After the environment creation, it is possible to activate it and
  deactivate it using the next commands:

  `conda activate data_engineer`

  `conda deactivate`

> NOTES:
> 
> * Get additional insights from the Anaconda Portal called
>   [Managing Environments](https://docs.conda.io/projects/conda/en/latest/user-guide/tasks/manage-environments.html#using-pip-in-an-environment).
> 
> * There is a lot of information and useful tips online; use the
>   [Conda Cheat Sheet](https://docs.conda.io/projects/conda/en/4.6.0/_downloads/52a95608c49671267e40c689e0bc00ca/conda-cheatsheet.pdf)
>   and this
>   [Towards Article](https://towardsdatascience.com/a-guide-to-conda-environments-bc6180fc533)
>   to get additional insights about Anaconda environments.

> **WARNING!!!**:
> 
> Install the management packages using **conda** and then use **pip**
> to install the development packages, it avoids unexpected behaviors,
> never try to use **conda** installer after using for the first time
> the command **pip** to install packages.
> <br><br>
> 
<br><br> <!-- Blank line -->

### Create Jupyter Kernel Environment

* Verify the Jupyter Notebook installation in the root environment:

  `conda install jupyter`

* Activate the development environment and install the Jupyter Kernel:

  `conda activate data_engineer`

  `conda install ipykernel`

* Link the Jupyter Kernel to Jupyter Notebook:

* `python -m ipykernel install --user --name data_engineer`
  `--display-name "Python v.3.9.6 (data_engineer)"`

* Launch the Jupyter Notebook, and check the new environment is
  available as a Jupyter Kernel option.

> NOTES:
> 
> * Get more insights about Jupyter Kernel Environments in this
>   [Stack Overflow Post](https://stackoverflow.com/questions/58068818/how-to-use-jupyter-notebooks-in-a-conda-environment).
> <br><br> <!-- Blank line -->
>
<br><br> <!-- Blank line -->

### Package Requirements

The package requirements are registered in their corresponding
**requirements.txt** file, this file is created manually alongside the
development process and in the same installation order.

It helps to avoid installation errors and incompatibilities at setting
environments on different machines.

The following command lines help to list correctly the package
requirements and versionig.

* List the installed packages and versions:

  `pip freeze`

  `pip list --format=freeze`

* If the **requirements.txt** file is completed or it is required to
  replicate the environment using the current development, use the next
  command line to install again the packages due to an environment
  crashing:

  `pip install -r requirements.txt`

  If the installation does not run correctly, use the next command to
  install one-by-one the required packages and versions following the
  **requirements.txt** packages and versions:

  `pip install <package>==<version_number>`

<br><br> <!-- Blank line -->

# Expected Results

The resulted tables are displayed bellow:

<!-- Original image size [1323 680] -->
<br/><br/> <!-- Blank line -->
<div align="center">
  <img width="860" height="442" src="img/nosql_tables.png"/>
</div>
<br/><br/> <!-- Blank line -->

The following images shows the final query responses inside the
`Data_Modeling_With_Cassandra.ipybb` notebook.

## Query 1

**Get the artist, song title and song's length in the music app history that was heard during sessionId 338, and itemInSession 4**

<!-- Original image size [464 71] -->
<br/><br/> <!-- Blank line -->
<div align="center">
  <img width="464" height="71" src="img/query_1_response.png"/>
</div>
<br/><br/> <!-- Blank line -->

## Query 2

**Get only the name of artist, song (sorted by itemInSession) and user (first and last name) for userid = 10, sessionid = 182**

<!-- Original image size [758 156] -->
<br/><br/> <!-- Blank line -->
<div align="center">
  <img width="758" height="156" src="img/query_2_response.png"/>
</div>
<br/><br/> <!-- Blank line -->

## Query 3

**Get every user name (first and last) in the music app history who listened to the song 'All Hands Against His Own'**

<!-- Original image size [285 133] -->
<br/><br/> <!-- Blank line -->
<div align="center">
  <img width="285" height="133" src="img/query_3_response.png"/>
</div>
<br/><br/> <!-- Blank line -->
